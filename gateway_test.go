/*
 * Copyright 2022 Eric Evans <eevans@wikimedia.org> and Wikimedia Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gateway

import (
	"regexp"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestQueryString(t *testing.T) {
	statement := SelectBuilder.
		Select(
			Column("artist").Get(),
			Column("album").Alias("compilation").Get(),
			Column("published").CastTo("timestamp").Get(),
			Column("track").Get(),
		).
		From("music", "tracks").
		Where("artist", EQ, "Van Halen").
		Where("album", EQ, "1984").
		Build()

	expected := normalizeQuery(`
		SELECT JSON 
			artist,
			album AS "compilation",
			cast(published AS timestamp),
			track
		FROM
			music.tracks
		WHERE
			artist = ? AND album = ?`)

	assert.Equal(t, expected, normalizeQuery(statement.String()))
}

var whitespaceRe *regexp.Regexp = regexp.MustCompile(`\s*`)

// Normalize a query for string comparison
func normalizeQuery(s string) string {
	return strings.ToLower(whitespaceRe.ReplaceAllString(s, ""))
}
